/*
  Copyright 2024, Sylvain Sauvage slswww@free.fr

  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty provided the copyright notice and
  this notice are preserved.  This file is offered as-is, without any
  warranty.
 */

#include <QImage>
#include <QuaZip-Qt6-1.4/quazip/quazipfile.h>
#include <KIO/ThumbnailCreator>
#include <KPluginFactory>

class StudIOThumbnailer: public KIO::ThumbnailCreator {
    Q_OBJECT

public:
    StudIOThumbnailer( QObject* parent, const QVariantList& args )
        : ThumbnailCreator( parent, args ) {}
    virtual ~StudIOThumbnailer() {}

    virtual KIO::ThumbnailResult create( const KIO::ThumbnailRequest& request ) override;
};

KIO::ThumbnailResult StudIOThumbnailer::create( const KIO::ThumbnailRequest& request )
{
    QuaZipFile qzf( request.url().path(), "thumbnail.png" );
    if ( qzf.getZipError() ) {
        return KIO::ThumbnailResult::fail();
    }
    if ( !qzf.open( QIODevice::ReadOnly ) ) {
        return KIO::ThumbnailResult::fail();
    }
    QByteArray data = qzf.readAll();
    if ( data.isEmpty() ) {
        // no data, retry with password
        qzf.close();
        if ( !qzf.open( QIODevice::ReadOnly, "soho0909" ) ) {
            return KIO::ThumbnailResult::fail();
        }
        data = qzf.readAll();
        if ( data.isEmpty() ) {
            return KIO::ThumbnailResult::fail();
        }
    }
    QImage preview;
    if ( !preview.loadFromData( data, "PNG" ) || preview.isNull() ) {
        return KIO::ThumbnailResult::fail();
    }
    return KIO::ThumbnailResult::pass( preview );
}

K_PLUGIN_CLASS_WITH_JSON( StudIOThumbnailer, "studiothumbnail.json" );
#include "studiothumb.moc"

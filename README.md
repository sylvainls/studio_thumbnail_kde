# studio_thumbnail_kde

studio_thumbnail_kde is a simple plugin for KDE 6 to create thumbnails for
BrickLink Studio model files.

# Requirements

cmake, kde & qt development files (libkf6kio-dev…), QuaZip-qt6.

# Build and install

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
sudo cmake --install .
kbuildsycoca6
```

# Licence

Copying and distribution of this project, with or without modification, are
permitted in any medium without royalty provided the copyright notice and this
notice are preserved.  This project is offered as-is, without any warranty.

# Trademarks

Studio & BrickLink are trademarks of the LEGO Group, which does not sponsor,
endorse, or authorize this project.
